﻿Key,Source,Context,English
ammo45BulletBall,items,Ammo,.45 Ammo
ammo45BulletAP,items,Ammo,AP .45 Ammo
ammo45BulletHP,items,Ammo,HiPower .45 Ammo
ammo45BulletBallDesc,items,Ammo,A .45 round used as firearm ammunition. Can be crafted at a workbench.
ammo45BulletAPDesc,items,Ammo,A .45 round used as firearm ammunition. Can be crafted at a workbench.
ammo45BulletHPDesc,items,Ammo,A .45 round used as firearm ammunition. Can be crafted at a workbench.
ammoBundle45BulletBall,items,Ammo,Box of .45 Ammo (75)
ammoBundle45BulletAP,items,Ammo,Box of .45 AP Ammo (75)
ammoBundle45BulletHP,items,Ammo,Box of .45 HP Ammo (75)
ammo556mmBulletBall,items,Ammo,5.56mm Ammo
ammo556mmBulletAP,items,Ammo,5.56mm AP Ammo
ammo556mmBulletHP,items,Ammo,HiPower 5.56mm Ammo
ammo556mmBulletBallDesc,items,Ammo,A 5.56mm round used as firearm ammunition. Can be crafted at a workbench.
ammo556mmBulletAPDesc,items,Ammo,A 5.56mm round used as firearm ammunition. Can be crafted at a workbench.
ammo556mmBulletHPDesc,items,Ammo,A 5.56mm round used as firearm ammunition. Can be crafted at a workbench.
ammoBundle556mmBulletBall,items,Ammo,Box of 5.56mm Ammo (120)
ammoBundle556mmBulletAP,items,Ammo,Box of AP 5.56mm Ammo (120)
ammoBundle556mmBulletHP,items,Ammo,Box of HP 5.56mm Ammo (120)