﻿Key,Source,Context,English
stoveKitRH,items,Item,Stove Repair Kit
stoveKitRHDesc,items,Item,"This stove repair kit is used with a hammer to take a broken stove and make it a working one that you can use."
workingModernStoveRH,blocks,Block,Working Modern Stove
workingModernStoveRHDesc,blocks,Block,"A working stove can be used to cook food and some chemicals in a safe environment."
workingOvenRH,blocks,Block,Working Oven
workingOvenRHDesc,blocks,Block,"A working stove can be used to cook food and some chemicals in a safe environment."
workingOldStoveRH,blocks,Block,Working Old Stove
workingOldStoveRHDesc,blocks,Block,"A working stove can be used to cook food and some chemicals in a safe environment."
workingStoveRHDesc,blocks,Block,"This stove can be repaired and used to cook food and some chemicals in a safe environment./n/nCraft a Stove Repair Kit and with a Hammer or Wrench right click an Oven or Stove to upgrade it to working."
StoveRepairKit,Items,Item,Wood Burning Stove Repair Kit
StoveRepairKitDesc,Items,Item,"Use it to repair old Wood Burning Stove and make it usable."
TOONStoveOld,Blocks,Block,Old Working Stove
TOONStoveOldDesc,Blocks,Block,"An old Wood Burning Stove to cook Food and some Chemicals at home."