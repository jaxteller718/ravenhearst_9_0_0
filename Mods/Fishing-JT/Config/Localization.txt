Key,Source,Context,English
foodRawMeat,items,Food,Raw Meat
foodCharredMeat,items,Food,Charred Meat
buffFoodStaminaBonusDesc,items,Food,Eating cooked food increases your max stamina. Higher quality foods like meat stew add a greater bonus.\n\nYour max stamina is increased by {cvar($foodStaminaBonus:0)}\n\nThe timer will not count down while your food bar is full. Will only overwrite a higher value buff if it's duration is below half.
foodMeatStewDesc,items,Food,A survivor isn't worth his salt if he can't make a meat stew.
foodMeatStewSchematic,items,Food,Meat Stew Recipe
foodFishTacos,items,Food,Fish Tacos
foodFishTacosSchematic,items,Food,Fish Tacos Recipe
h7sb_fishingRod,items,Food,Fishing Pole
h7sb_fishingRodDesc,items,Food,"To use the fishing rod stand by the water or on a raft and use a bait. As soon as the bait buff appears you can cast the fishing rod (LMB). \n\nOnce a fish has bitten pay attention to it in which direction it swims (left or right). Pull the fishing rod in the opposite direction (LMB/RMB). Repeat the process until the fish is caught and you can use a fishing net."
h7sb_maggot,items,Food,Maggot
h7sb_maggotDesc,items,Food,"Maggots can be found in carcasses and used as fishing bait. \n\nIn an emergency, you can eat them directly."
h7sb_bait,items,Food,Fishing Bait
h7sb_baitDesc,items,Food,"Fishing baits can be used (RMB) to catch fish. \n\nAfter the buff appears, quickly use a fishing rod before time runs out (60 seconds) and the bait stops squirming."
bait,buffs,Buff,Fishing Bait
baitDesc,buffs,Buff,"As long as the fishing bait squirming, you can cast a line to catch a fish."
h7sb_fishingNet,items,Food,Fishing Net
h7sb_fishingNetDesc,items,Food,"The fishing net can be used (LMB/RMB) to catch caught fish. \n\nUse LMB for small fish. \nUse RMB for big fish."
h7sb_fishDeco,blocks,Block,Fish (Deco)
h7sb_fishDecoDesc,blocks,Block,"A decorative fish of no further use that you learn how to make after you have caught your first fish. \n\nWarning! This block has no hitbox, ie you must destroy the block below it (and maybe surrounding blocks) in order to remove the fish. The item will be lost."
Raw Fish,items,Item Food,Raw Fish
foodRawFish,items,Food,Fish,Rohes Fleisch
rawFishDesc,items,Food,"Fish can be eaten raw or fillet, but it is better to collect 10 of them and prepare a fish farm."."
FishOven,blocks,Block,Oakraven Fish Oven
FishOvenDesc,blocks,Block,"The Oakraven Fish Oven allows players to cook their Fish Farm harvest into a variety of recipes."
FishFarmEmpty,blocks,Block,Oakraven Fish Farm (Empty)
FishFarmEmptyDesc,blocks,Block,"Place this Oakraven Fish Farm in an appropriate location, and add an Oakraven Fish Growing Kit to begin the fish farming process."
FishFarmEggs,blocks,Block,Oakraven Fish Farm (Egg Incubation Stage)
FishFarmHatched,blocks,Block,Oakraven Fish Farm (Hatching Stage)
FishFarmBreeding,blocks,Block,Oakraven Fish Farm (Growth and Breeding Stage)
FishFarmFishGrown,blocks,Block,Oakraven Fish Farm (Harvest Stage)
Salmon,blocks,Block,Salmon
SalmonDesc,blocks,Block,"Place this Salmon on the floor and harvest. It can be used as part of a recipe."
Barramundi,blocks,Block,Barramundi
BarramundiDesc,blocks,Block,"Place this Barramundi on the floor and harvest. It can be used as part of a recipe."
Tangfish,blocks,Block,Tang Fish
TangfishDesc,blocks,Block,"Place this Tang Fish on the floor and harvest. It can be used as part of a recipe."
MutantPiranha,blocks,Block,Mutant Piranha
MutantPiranhaDesc,blocks,Block,"Place this Mutant Piranha on the floor and harvest. It can be used as part of a recipe."
PerchFish,blocks,Block,Perch Fish
PerchFishDesc,blocks,Block,"Place this Perch Fish on the floor and harvest. It can be used as part of a recipe."
ShinerFish,blocks,Block,Shiner Fish
ShinerFishDesc,blocks,Block,"Place this Shiner Fish on the floor and harvest. It can be used as part of a recipe."
ArramusFish,blocks,Block,Navezgane Fish
ArramusFishDesc,blocks,Block,"Place this Navezgane Fish on the floor and harvest. It can be used as part of a recipe."
BlueFinTuna,blocks,Block,Bluefin Tuna Fish
BlueFinTunaDesc,blocks,Block,"Place this Bluefin Tuna Fish on the floor and harvest. It can be used as part of a recipe."

FishGrowingKit,items,Item,Oakraven Fish Farming Kit
FishGrowingKitDesc,items,Item,"Place the Oakraven Fish Farming Kit in your empty Oakraven Fish Farm to begin the fish farming process."
FishFillet,items,Item,Fish Fillet
FishFilletDesc,items,Item,"This Fish Fillet can be added to a recipe to make a fish dish."
foodFishmeringue,items,Item,Fish Meringue
foodFishmeringueDesc,items,Item,"Fish Meringue uses farmed fish to provide players with a healthy and nutritious dish."
foodFishSpinePie,items,Item,Fish Pie
foodFishSpinePieDesc,items,Item,"Fish Pie uses farmed fish to provide players with a healthy and nutritious dish."
foodFishSoup,items,Item,Fish Soup
foodFishSoupDesc,items,Item,"Fish Soup uses farmed fish to provide players with a healthy and nutritious dish."
foodFishStew,items,Item,Fish Stew
foodFishStewDesc,items,Item,"Fish Stew uses farmed fish to provide players with a healthy and nutritious dish."
foodPerchPie,items,Item,Perch Pie
foodPerchPieDesc,items,Item,"Perch Pie uses farmed fish to provide players with a healthy and nutritious dish."
foodFishCurry,items,Item,Fish Curry
foodFishCurryDesc,items,Item,"Fish Curry uses farmed fish to provide players with a healthy and nutritious dish."
SearedSalmonFillet,items,Item,Seared Salmon Fillet
SearedSalmonFilletDesc,items,Item,"This Seared Salmon Fillet uses farmed fish to provide players with a healthy and nutritious dish."
FreshSalmonFillet,items,Item,Fresh Salmon Fillet
FreshSalmonFilletDesc,items,Item,"This Fresh Salmon Fillet can be seared or used as part of a recipe."
FreshTunaFillet,items,Item,Fresh Bluefin Tuna Fillet
FreshTunaFilletDesc,items,Item,"This Fresh Bluefin Tuna Fillet can be used as part of a recipe."
PiranhaOnAStick,items,Item,Piranha On A Stick
PiranhaOnAStickDesc,items,Item,"Piranha On A Stick uses farmed fish to provide players with a healthy and nutritious treat. Use the left mouse button to eat the the right mounse button to attack."
PerchOnAStick,items,Item,Perch On A Stick
PerchOnAStickDesc,items,Item,"Perch On A Stick uses farmed fish to provide players with a healthy and nutritious treat. Use the left mouse button to eat the the right mounse button to attack."
barramundiOnAStick,items,Item,Barramundi On A Stick
barramundiOnAStickDesc,items,Item,"Barramundi On A Stick uses farmed fish to provide players with a healthy and nutritious treat. Use the left mouse button to eat the the right mounse button to attack."
ShinerFishOnAStick,items,Item,Shiner Fish On A Stick
ShinerFishOnAStickDesc,items,Item,"Shiner Fish On A Stick uses farmed fish to provide players with a healthy and nutritious treat. Use the left mouse button to eat the the right mounse button to attack."
tangfishOnAStick,items,Item,Tang Fish On A Stick
tangfishOnAStickDesc,items,Item,"Tang Fish On A Stick uses farmed fish to provide players with a healthy and nutritious treat. Use the left mouse button to eat the the right mounse button to attack."
ArramusOnAStick,items,Item,Navezgane Fish On A Stick
ArramusOnAStickDesc,items,Item,"Navezgane Fish On A Stick uses farmed fish to provide players with a healthy and nutritious treat. Use the left mouse button to eat the the right mounse button to attack."
FishStick,items,Item,Fish Stick
FishStickDesc,items,Item,"The Fish Stick is used as part of a recipe, or can also provide players with a melee weapon."
