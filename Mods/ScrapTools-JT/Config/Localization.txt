Key,Source,Context,English
meleeToolAxeT0ScrapAxeRH,items,Tool,Scrap Axe
meleeToolAxeT0ScrapAxeRHDesc,items,Tool,The scrap axe is useful for harvesting wood.\n\nRepair with Duct Tape.\nScrap to Iron.
meleeToolPickT0ScrapPickaxeRH,items,Tool,Scrap Pickaxe
meleeToolPickT0ScrapPickaxeRHDesc,items,Tool,The scrap pickaxe is useful for breaking stone and harvesting minerals.\n\nRepair with Duct Tape.\nScrap to Iron.
meleeWpnBladeT0ScrapKnifeRH,items,Tool,Scrap Knife
meleeWpnBladeT0ScrapKnifeRHDesc,items,Tool,The scrap knife is useful for slashing zombies and gutting animals for meat.\nRegular attacks cause 1 Bleeding Wound and power attacks at least 2.\nRepair with Duct Tape.\nScrap to Iron.
meleeToolPickT0ScrapPickaxeRHDesc,items,Tool,The scrap pickaxe is useful for breaking stone and harvesting minerals.\n\nRepair with Duct Tape.\nScrap to Iron.
meleeToolShovelT0ScrapShovelRH,items,Tool,Scrap Shovel
meleeToolShovelT0ScrapShovelRHDesc,items,Tool,"The scrap shovel is useful for digging dirt, sand or snow.\nRepair with Duct Tape.\nScrap to Iron."