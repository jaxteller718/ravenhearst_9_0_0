Key,English
vehicleBoxTruckbody,Box Truck Body
vehicleBoxTruckbodyDesc,Parts needed to craft the Box Truck.
vehicleBoxTruckchassis,Box Truck Chassis
vehicleBoxTruckchassisDesc,Parts needed to craft the Box Truck.
vehicleBoxTruckPlain,Box Truck
vehicleBoxTruckPlainplaceable,Box Truck
vehicleBoxTruckPlainplaceableDesc,An old Box Truck.
vehicleBoxTruckHostess,Hostess Box Truck
vehicleBoxTruckHostessplaceable,Hostess Box Truck
vehicleBoxTruckHostessplaceableDesc,Tallahassee's favorite!
vehicleBoxTruckchassisSchematic,Box Truck Chassis Schematic
vehicleBoxTruckbodySchematic,Box Truck Body Schematic